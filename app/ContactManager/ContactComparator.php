<?php

namespace App\ContactManager;

class ContactComparator
{
    public function compare(Contact $contact1, Contact $contact2)
    {
        $matches = 0;
        $namesMatch = false;
        $surnamesMatch = false;

        if($contact1->getName() === $contact2->getName())
        {
            $namesMatch = true;
            $matches++;
        }

        if($contact1->getSurname() === $contact2->getSurname())
        {
            $surnamesMatch = true;
            $matches++;
        }

        if($contact1->getAge() === $contact2->getAge())
        {
            $matches++;
        }

        if($contact1->getCity() === $contact2->getCity())
        {
            $matches++;
        }                 

        return $matches>=3 && $namesMatch && $surnamesMatch ? true : false; 
    }
}
