<?php

namespace App\ContactManager;

class ContactValidator
{
    public function validate(Contact $contact)
    {
        //RULE 1 - name can't be empty'
        if($contact->getName() === "")
        {
            return false;
        }
        /* TODO - add 2 rules more */
        //RULE 2 - surname can't be empty
        if($contact->getSurname() === "")
        {
            return false;
        }        

        //RULE 3 - age must be gigger than 18

        return true;
    }
}
