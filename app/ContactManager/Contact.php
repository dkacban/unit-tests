<?php

namespace App\ContactManager;

class Contact
{
    public function __construct($name, $surname, $age, $city)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->age = $age;
        $this->city = $city;                        
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function getCity()
    {
        return $this->city;
    }        

    private $name;
    private $surname;
    private $age;
    private $city; 
}