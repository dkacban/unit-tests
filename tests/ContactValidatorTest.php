<?php

use App\ContactManager\ContactValidator;
use App\ContactManager\Contact;

class ContactValidatorTest extends TestCase
{
    public function testValidate_WhenContactIsValid()
    {
        $validator = new ContactValidator();
        $validContact = new Contact("Darek", "Kacban", 29, "Piła");
        $result = $validator->validate($validContact);

        $this->assertEquals(true, $result);
    }

    public function testValidate_WhenNameIsEmpty_ThenShouldBeFalse()
    {
        $validator = new ContactValidator();
        $validContact = new Contact("", "Kacban", 29, "Piła");
        $result = $validator->validate($validContact);

        $this->assertEquals(false, $result);
    }    


    public function testValidate_WhenSurnameIsEmpty_ThenShouldBeFalse()
    {
        $validator = new ContactValidator();
        $validContact = new Contact("Darek", "", 29, "Piła");
        $result = $validator->validate($validContact);

        $this->assertEquals(false, $result);        
    }

}
