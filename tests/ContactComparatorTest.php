<?php

use App\ContactManager\ContactComparator;
use App\ContactManager\Contact;

class ContactComparatorTest extends TestCase
{
    public function testCompare_WhenAllPropertiesAreIdentical_ShouldBeTrue()
    {
        //ARRANGE
        $comparator = new ContactComparator();

        $contact1 = new Contact("Darek", "Kacban", 29, "Piła");
        $contact2 = new Contact("Darek", "Kacban", 29, "Piła");

        //ACT
        $areIdentical = $comparator->compare($contact1, $contact2);

        //ASSERT
        $this->assertEquals(true, $areIdentical);
    }

    public function testCompare_WhenZeroPropertiesAreIdentical_ShouldBeFalse()
    {
        $comparator = new ContactComparator();
        $contact1 = new Contact("Adam", "Nowak", 26, "Warszawa");
        $contact2 = new Contact("Darek", "Kacban", 29, "Piła");

        $areIdentical = $comparator->compare($contact1, $contact2);

        $this->assertEquals(false, $areIdentical);
    }

    public function testCompare_WhenThreePropertiesAreIdentical_ShouldBeTrue()
    {
        $comparator = new ContactComparator();
        $contact1 = new Contact("Darek", "Kacban", 27, "Piła");
        $contact2 = new Contact("Darek", "Kacban", 29, "Piła");

        $areIdentical = $comparator->compare($contact1, $contact2);

        $this->assertEquals(true, $areIdentical);        
    }
    public function testCompare_WhenTwoPropertiesAreIdentical_ShouldBeFalse()
    {
        $comparator = new ContactComparator();
        $contact1 = new Contact("Jan", "Kacban", 28, "Piła");
        $contact2 = new Contact("Darek", "Kacban", 29, "Piła");

        $areIdentical = $comparator->compare($contact1, $contact2);

        $this->assertEquals(false, $areIdentical);        
    }

    public function testCompare_WhenOnePropertyIsIdentical_ShouldBeFalse()
    {
        $comparator = new ContactComparator();
        $contact1 = new Contact("Darek", "Kowalski", 28, "Ujście");
        $contact2 = new Contact("Darek", "Kacban", 29, "Piła");

        $areIdentical = $comparator->compare($contact1, $contact2);

        $this->assertEquals(false, $areIdentical); 
    }
}